import { Document, Schema, Model, model } from "mongoose";

export const PostSchema = new Schema({
    createdAt: Date,
    text: {
        type: String,
        required: true
    } ,
    user: {
        type: String,
        required: true
    },
    ref_num: String
});