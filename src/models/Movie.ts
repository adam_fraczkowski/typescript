import { Document, Schema, Model, model } from "mongoose";


export const MovieSchema = new Schema({
    createdAt: Date,
    title: String,
    year: String,
    rated: String,
    released: Date,
    runtime: Number,
    genre: [String],
    director: [String],
    writer: [String],
    actors: [String],
    plot: String,
    language: [String],
    contury: [String],
    production: String,
    imdbid: {
        type: String,
        unique: true
    },
    ratings: Schema.Types.Mixed,
    imdbrating: Number,
    imdbvotes: Number,
    ident: String
});