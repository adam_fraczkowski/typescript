import { MovieSchema } from "./../models/Movie";
import { PostSchema } from "./../models/Post";
import mongoose from "mongoose";
import { v4 as uuid } from "uuid";

/*General DB Interface. This is for mongoose. If you want to add other DBs engines  import schema and provide DBInterface with this methods*/

export default class DBInterface {

    private MovieCollection: any;
    private PostCollection: any;

    constructor(connectionUri: string) {

        mongoose.connect(connectionUri, (err) => {
            if (err) {
                console.error(err);
                console.log(err);
                process.exit(1);
                throw Error("Error during connection:" + err );

            }
        });
        this.MovieCollection = mongoose.model("MovieInterface", MovieSchema);
        this.PostCollection = mongoose.model("PostInterface", PostSchema);
    }

    async saveMovie(Obj: any) {
        if ( Obj.ident == undefined) Obj.ident = uuid();
        if ( Obj.createdAt == undefined ) Obj.createdAt = new Date();
        if ( Obj.imdbid == undefined ) throw Error( "Object to save is incorrect" );
        try {
             await this.MovieCollection.create({
                createdAt: Obj.createdAt,
                title: Obj.title,
                year: Obj.year,
                rated: Obj.rated,
                released: Obj.released,
                runtime: Obj.runtime,
                genre: Obj.genre,
                director: Obj.director,
                writer: Obj.writer,
                actors: Obj.actors,
                plot: Obj.plot,
                language: Obj.language,
                contury: Obj.contury,
                production: Obj.production,
                imdbid: Obj.imdbid,
                ratings: Obj.ratings,
                imdbrating: Obj.imdbrating,
                imdbvotes: Obj.imdbvotes,
                ident: Obj.ident
            });
            return true;
        } catch (error) {
            throw Error("Error during movie save: " + error);
        }
    }

    async getMovies() {
        try {
            const results = await this.MovieCollection.find({}).exec();
            return JSON.parse(JSON.stringify(results));
        } catch ( error ) {
            throw new Error("Error during get Movies from db: " + error);
        }
    }

    async savePost(Obj: any) {
        if (Obj.user == undefined || Obj.text == undefined) throw Error("Incorrect Post object to save" );
        if (Obj.createdAt == undefined ) Obj.createdAt = new Date();
        try {
            await this.PostCollection.create({
                user: Obj.user,
                text: Obj.text,
                createdAt: Obj.createdAt,
                ref_num: Obj.ref_num

            });
            return true;
        } catch (error) {
            throw Error("Error during Post save " + error);
        }
    }

    async getPosts() {
        try {
            const results = await this.PostCollection.find({}).exec();
            return JSON.parse(JSON.stringify(results));
        } catch (error) {
            throw Error("Error during getting posts " + error);
        }
    }

    async removeMovies() {
        try {
            await this.MovieCollection.deleteMany({});
            return true;
        } catch (error) {
            throw Error("Error during remove movies: " + error);
        }
    }

    async removePosts() {
        try {
            await this.PostCollection.deleteMany({});
            return true;
        } catch (error) {
            throw Error("Error during remove posts: " + error);
        }
    }

}