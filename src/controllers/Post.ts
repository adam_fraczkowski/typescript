export default class Post {

    private dbHandler: any;

    private user: string;
    private text: string;
    private ref_num: string;

    constructor(dbHandler: any, user?: string, text?: string, ref_num?: string) {
        this.dbHandler = dbHandler;
        this.user = user;
        this.text = text;
        if (ref_num != undefined) this.ref_num = ref_num;
        else this.ref_num = "0";
    }


    async getPosts() {
        const results = await this.dbHandler.getPosts();
        const parsedResult = results.filter((element: any ) => {
            delete element._id;
            delete element.__v;
            return element;
        });
        return parsedResult;
    }

    async savePost()  {
        await this.dbHandler.savePost(this);
        return true;
    }

}