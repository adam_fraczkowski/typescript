import * as request from "request-promise";


export default class Movie {

    private apikey: string;
    private url: string;
    private dbHandler: any;

    public title: string;
    private year: string;
    private rated: string;
    private released: Date;
    private runtime: number;
    private genre: string[];
    private director: string[];
    private writer: string[];
    private actors: string[];
    private plot: string;
    private language: string[];
    private contury: string[];
    private production: string;
    public imdbid: string;
    private ratings: any[];
    private imdbrating: number;
    private imdbvotes: number;
    private ident: string;


    /* gets two params - dbHandler and apiKey */

    constructor(dbHandler: any, apiKey: string) {
            this.apikey = apiKey;
            this.dbHandler = dbHandler;
            this.url = "http://www.omdbapi.com/?apikey=" + this.apikey + "&";

    }

    /* Parse JSON object to class instance
    * jsonData - json object
    * return true or throw error
    */
    async apiClassParser(jsonData: any) {
        try {
            this.title = jsonData["Title"];
            this.year = jsonData["Year"];
            this.rated = jsonData["Rated"];
            this.director = jsonData["Director"].split(",");
            this.released = new Date(jsonData["Released"]);
            this.runtime = parseInt(jsonData["Runtime"]);
            this.genre = jsonData["Genre"].split(",");
            this.writer = jsonData["Writer"].split(",");
            this.actors = jsonData["Actors"].split(",");
            this.plot = jsonData["Plot"];
            this.language = jsonData["Language"].split(",");
            this.contury = jsonData["Country"].split(",");
            this.production = jsonData["Production"];
            this.imdbid = jsonData["imdbID"];
            this.ratings = jsonData["Ratings"];
            this.imdbrating = parseFloat(jsonData["imdbRating"]);
            this.imdbvotes = parseInt(jsonData["imdbVotes"]);
            return true;
        } catch (error) {
            throw Error("Cannot parse data to Movie instance:" + error);
        }
    }
    /*fill data from OMDb Api. Query depends on params like:
    *   imdbID - IMDB service ID
    *   title  - movie title
    *   search - search phrase
    *   returns true, search results or throws error
    */
    async fillFromApi(imdbID?: string, title?: string, search?: string) {
        let urlSuffix = "";
        let responseAPI = undefined;
        if ( this.url == undefined ) throw Error("Url should be provided" );
        if ( imdbID != undefined ) urlSuffix = "i=" + imdbID;
        else if ( title != undefined ) urlSuffix = "t=" + title;
        else if ( search != undefined ) urlSuffix = "s=" + search;
        else {
            throw Error("Invalid params in fillFormApi function");
        }
        try {
            responseAPI = await request.get(this.url + urlSuffix);
            responseAPI = JSON.parse(responseAPI);
        } catch (error) {
            throw Error("Error during fetch data from API:" + error);
        }
        if ( responseAPI["Response"] == "False" ) throw Error( responseAPI["Error"] );
        else if ( search != undefined ) return responseAPI["Search"];
        else {
            const parserResult = await this.apiClassParser(responseAPI);
            if ( parserResult ) {
                return true;
            } else {
                return false;
            }
        }
    }
    /*
     * save class instance to database using dbHandler provided in constructor
     */
    async saveMovieToDb() {
        const result = await this.dbHandler.saveMovie(this);
        return result;
    }

    /*
     * getting list from db. Returns JSON array
     */

    async getMoviesFromDb() {
        const result = await this.dbHandler.getMovies();
        // parse array
        const parsedResult = result.filter((element: any ) => {
            delete element._id;
            delete element.__v;
            return element;
        });
        return parsedResult;
    }

}