import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";

import Movie from "./controllers/Movie";
import Post  from "./controllers/Post";
import DBInterface from "./db/DBInterface";


const app = express();


const database = process.env.DB || "mongodb://127.0.0.1:27017/production";
const apiKey = process.env.API_KEY || "27b7ad6e";


const DB = new DBInterface(database);



app.use(bodyParser.json());
app.use(helmet());



app.get("/movies", async (req, res, next) => {
    try {
        const MovieObject = new Movie(DB, apiKey);
        const result = await MovieObject.getMoviesFromDb();
        res.status(200).send(result);
    } catch (error) {
        next(error);
    }
});

app.get("/comments", async (req, res, next) => {
    try {
        const PostObject = new Post(DB);
        const result = await PostObject.getPosts();
        res.status(200).send(result);
    } catch (error) {
        next(error);
    }
});

app.post("/movies", async (req , res, next) => {
   try {
        const MovieObject = new Movie(DB, apiKey);
        const result = await MovieObject.fillFromApi(req.body.imdbID, req.body.title, undefined);
        const putToDB = await MovieObject.saveMovieToDb();
        res.status(200).send("OK");
   } catch (error) {
       next(error);
   }
});

app.post("/comments", async (req, res, next) => {
    try {
        const PostObject = new Post(DB, req.body.user, req.body.text, req.body.ref_num);
        const result = await PostObject.savePost();
        res.status(200).send("OK");
    } catch (error) {
        next(error);
    }
});

app.use((err: any, req: any, res: any, next: any) => {
    console.error(err.stack);
    res.status(500).send({error: err + " Details in log"});
});

export default app;