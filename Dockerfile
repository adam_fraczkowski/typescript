FROM node:10-alpine

ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*","./"]
COPY ["dist","./"]
RUN npm install 
RUN npm install --production --silent 
EXPOSE 8080
CMD node server.js