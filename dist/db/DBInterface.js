"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Movie_1 = require("./../models/Movie");
const Post_1 = require("./../models/Post");
const mongoose_1 = __importDefault(require("mongoose"));
const uuid_1 = require("uuid");
/*General DB Interface. This is for mongoose. If you want to add other DBs engines  import schema and provide DBInterface with this methods*/
class DBInterface {
    constructor(connectionUri) {
        mongoose_1.default.connect(connectionUri, (err) => {
            if (err) {
                console.error(err);
                console.log(err);
                process.exit(1);
                throw Error("Error during connection:" + err);
            }
        });
        this.MovieCollection = mongoose_1.default.model("MovieInterface", Movie_1.MovieSchema);
        this.PostCollection = mongoose_1.default.model("PostInterface", Post_1.PostSchema);
    }
    saveMovie(Obj) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Obj.ident == undefined)
                Obj.ident = uuid_1.v4();
            if (Obj.createdAt == undefined)
                Obj.createdAt = new Date();
            if (Obj.imdbid == undefined)
                throw Error("Object to save is incorrect");
            try {
                yield this.MovieCollection.create({
                    createdAt: Obj.createdAt,
                    title: Obj.title,
                    year: Obj.year,
                    rated: Obj.rated,
                    released: Obj.released,
                    runtime: Obj.runtime,
                    genre: Obj.genre,
                    director: Obj.director,
                    writer: Obj.writer,
                    actors: Obj.actors,
                    plot: Obj.plot,
                    language: Obj.language,
                    contury: Obj.contury,
                    production: Obj.production,
                    imdbid: Obj.imdbid,
                    ratings: Obj.ratings,
                    imdbrating: Obj.imdbrating,
                    imdbvotes: Obj.imdbvotes,
                    ident: Obj.ident
                });
                return true;
            }
            catch (error) {
                throw Error("Error during movie save: " + error);
            }
        });
    }
    getMovies() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const results = yield this.MovieCollection.find({}).exec();
                return JSON.parse(JSON.stringify(results));
            }
            catch (error) {
                throw new Error("Error during get Movies from db: " + error);
            }
        });
    }
    savePost(Obj) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Obj.user == undefined || Obj.text == undefined)
                throw Error("Incorrect Post object to save");
            if (Obj.createdAt == undefined)
                Obj.createdAt = new Date();
            try {
                yield this.PostCollection.create({
                    user: Obj.user,
                    text: Obj.text,
                    createdAt: Obj.createdAt,
                    ref_num: Obj.ref_num
                });
                return true;
            }
            catch (error) {
                throw Error("Error during Post save " + error);
            }
        });
    }
    getPosts() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const results = yield this.PostCollection.find({}).exec();
                return JSON.parse(JSON.stringify(results));
            }
            catch (error) {
                throw Error("Error during getting posts " + error);
            }
        });
    }
    removeMovies() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.MovieCollection.deleteMany({});
                return true;
            }
            catch (error) {
                throw Error("Error during remove movies: " + error);
            }
        });
    }
    removePosts() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.PostCollection.deleteMany({});
                return true;
            }
            catch (error) {
                throw Error("Error during remove posts: " + error);
            }
        });
    }
}
exports.default = DBInterface;
//# sourceMappingURL=DBInterface.js.map