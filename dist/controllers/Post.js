"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Post {
    constructor(dbHandler, user, text, ref_num) {
        this.dbHandler = dbHandler;
        this.user = user;
        this.text = text;
        if (ref_num != undefined)
            this.ref_num = ref_num;
        else
            this.ref_num = "0";
    }
    getPosts() {
        return __awaiter(this, void 0, void 0, function* () {
            const results = yield this.dbHandler.getPosts();
            const parsedResult = results.filter((element) => {
                delete element._id;
                delete element.__v;
                return element;
            });
            return parsedResult;
        });
    }
    savePost() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.dbHandler.savePost(this);
            return true;
        });
    }
}
exports.default = Post;
//# sourceMappingURL=Post.js.map