"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = __importStar(require("request-promise"));
class Movie {
    /* gets two params - dbHandler and apiKey */
    constructor(dbHandler, apiKey) {
        this.apikey = apiKey;
        this.dbHandler = dbHandler;
        this.url = "http://www.omdbapi.com/?apikey=" + this.apikey + "&";
    }
    /* Parse JSON object to class instance
    * jsonData - json object
    * return true or throw error
    */
    apiClassParser(jsonData) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.title = jsonData["Title"];
                this.year = jsonData["Year"];
                this.rated = jsonData["Rated"];
                this.director = jsonData["Director"].split(",");
                this.released = new Date(jsonData["Released"]);
                this.runtime = parseInt(jsonData["Runtime"]);
                this.genre = jsonData["Genre"].split(",");
                this.writer = jsonData["Writer"].split(",");
                this.actors = jsonData["Actors"].split(",");
                this.plot = jsonData["Plot"];
                this.language = jsonData["Language"].split(",");
                this.contury = jsonData["Country"].split(",");
                this.production = jsonData["Production"];
                this.imdbid = jsonData["imdbID"];
                this.ratings = jsonData["Ratings"];
                this.imdbrating = parseFloat(jsonData["imdbRating"]);
                this.imdbvotes = parseInt(jsonData["imdbVotes"]);
                return true;
            }
            catch (error) {
                throw Error("Cannot parse data to Movie instance:" + error);
            }
        });
    }
    /*fill data from OMDb Api. Query depends on params like:
    *   imdbID - IMDB service ID
    *   title  - movie title
    *   search - search phrase
    *   returns true, search results or throws error
    */
    fillFromApi(imdbID, title, search) {
        return __awaiter(this, void 0, void 0, function* () {
            let urlSuffix = "";
            let responseAPI = undefined;
            if (this.url == undefined)
                throw Error("Url should be provided");
            if (imdbID != undefined)
                urlSuffix = "i=" + imdbID;
            else if (title != undefined)
                urlSuffix = "t=" + title;
            else if (search != undefined)
                urlSuffix = "s=" + search;
            else {
                throw Error("Invalid params in fillFormApi function");
            }
            try {
                responseAPI = yield request.get(this.url + urlSuffix);
                responseAPI = JSON.parse(responseAPI);
            }
            catch (error) {
                throw Error("Error during fetch data from API:" + error);
            }
            if (responseAPI["Response"] == "False")
                throw Error(responseAPI["Error"]);
            else if (search != undefined)
                return responseAPI["Search"];
            else {
                const parserResult = yield this.apiClassParser(responseAPI);
                if (parserResult) {
                    return true;
                }
                else {
                    return false;
                }
            }
        });
    }
    /*
     * save class instance to database using dbHandler provided in constructor
     */
    saveMovieToDb() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this.dbHandler.saveMovie(this);
            return result;
        });
    }
    /*
     * getting list from db. Returns JSON array
     */
    getMoviesFromDb() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this.dbHandler.getMovies();
            // parse array
            const parsedResult = result.filter((element) => {
                delete element._id;
                delete element.__v;
                return element;
            });
            return parsedResult;
        });
    }
}
exports.default = Movie;
//# sourceMappingURL=Movie.js.map