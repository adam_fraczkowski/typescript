"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const helmet_1 = __importDefault(require("helmet"));
const Movie_1 = __importDefault(require("./controllers/Movie"));
const Post_1 = __importDefault(require("./controllers/Post"));
const DBInterface_1 = __importDefault(require("./db/DBInterface"));
const app = express_1.default();
const database = process.env.DB || "mongodb://127.0.0.1:27017/production";
const apiKey = process.env.API_KEY || "27b7ad6e";
const DB = new DBInterface_1.default(database);
app.use(body_parser_1.default.json());
app.use(helmet_1.default());
app.get("/movies", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const MovieObject = new Movie_1.default(DB, apiKey);
        const result = yield MovieObject.getMoviesFromDb();
        res.status(200).send(result);
    }
    catch (error) {
        next(error);
    }
}));
app.get("/comments", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const PostObject = new Post_1.default(DB);
        const result = yield PostObject.getPosts();
        res.status(200).send(result);
    }
    catch (error) {
        next(error);
    }
}));
app.post("/movies", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const MovieObject = new Movie_1.default(DB, apiKey);
        const result = yield MovieObject.fillFromApi(req.body.imdbID, req.body.title, undefined);
        const putToDB = yield MovieObject.saveMovieToDb();
        res.status(200).send("OK");
    }
    catch (error) {
        next(error);
    }
}));
app.post("/comments", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const PostObject = new Post_1.default(DB, req.body.user, req.body.text, req.body.ref_num);
        const result = yield PostObject.savePost();
        res.status(200).send("OK");
    }
    catch (error) {
        next(error);
    }
}));
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send({ error: err + " Details in log" });
});
exports.default = app;
//# sourceMappingURL=app.js.map