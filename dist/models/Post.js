"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.PostSchema = new mongoose_1.Schema({
    createdAt: Date,
    text: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    },
    ref_num: String
});
//# sourceMappingURL=Post.js.map