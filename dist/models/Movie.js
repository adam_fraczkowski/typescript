"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.MovieSchema = new mongoose_1.Schema({
    createdAt: Date,
    title: String,
    year: String,
    rated: String,
    released: Date,
    runtime: Number,
    genre: [String],
    director: [String],
    writer: [String],
    actors: [String],
    plot: String,
    language: [String],
    contury: [String],
    production: String,
    imdbid: {
        type: String,
        unique: true
    },
    ratings: mongoose_1.Schema.Types.Mixed,
    imdbrating: Number,
    imdbvotes: Number,
    ident: String
});
//# sourceMappingURL=Movie.js.map