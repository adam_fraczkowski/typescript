import  request from "supertest";
import  Movie  from "../src/controllers/Movie";
import  Post from "../src/controllers/Post";
import  server from "./../src/app";

import DBInterface from "../src/db/DBInterface";

let DB_STRING = process.env.DATABASE || "mongodb://localhost:27017/test";

let API_KEY = "27b7ad6e";
let EXAMPLE_TITLE = "Blade Runner";
let EXAMPLE_IMDB = "tt0083658";
let WRONG_EXAMPLE_TITLE = "undefined title";
let WRONG_EXAMPLE_IMDB = "0";
let DB_HANDLER = new DBInterface(DB_STRING);

describe("Movie class", () => {

    beforeEach(async () => await DB_HANDLER.removeMovies() );

    test("get movie data from API", async () => {
        let movieObj = new Movie(DB_HANDLER, API_KEY);
        let result = await movieObj.fillFromApi(undefined, "Blade Runner", undefined);
        expect(movieObj.title).toBe(EXAMPLE_TITLE);
        expect(result).toBe(true);
    });

    test("get Movie object from external API  by imdbID", async () => {
        let movieObj = new Movie(DB_HANDLER, API_KEY);
        let result = await movieObj.fillFromApi(EXAMPLE_IMDB, undefined, undefined);
        expect(movieObj.title).toBe(EXAMPLE_TITLE);
        expect(result).toBe(true);
    });
    test("get Movie from external API and save in database", async() => {
        let movieObj = new Movie(DB_HANDLER,API_KEY);
        let result = await movieObj.fillFromApi(undefined, EXAMPLE_TITLE, undefined );
        expect(result).toBe(true);
        let databaseRes = await movieObj.saveMovieToDb();
        expect(databaseRes).toBe(true);
    });

    test("get all movies from database", async() => {
        let movieObj = new Movie(DB_HANDLER, API_KEY);
        //put two movies to database
        await movieObj.fillFromApi(undefined, "American Beauty", undefined);
        await movieObj.saveMovieToDb();
        await movieObj.fillFromApi(undefined, "Forrest Gump", undefined);
        await movieObj.saveMovieToDb()
        let result = await movieObj.getMoviesFromDb();
        expect(result).toBeInstanceOf(Array);
    } );

});

describe("Posts class", async () => {

    beforeEach(async () => DB_HANDLER.removePosts())

    test("put posts to database", async() => {
        let FirstPost = new Post(DB_HANDLER, "Adam", "First Comment", "ref_example");
        let SecondPost = new Post(DB_HANDLER, "Matt", "Awesome Movie");
        let ThirdPost = new Post(DB_HANDLER, "Steven", "Boring...");
        let resOne = await FirstPost.savePost();
        let resTwo = await SecondPost.savePost();
        let resThree = await ThirdPost.savePost();
        expect(resOne).toBe(true);
        expect(resTwo).toBe(true);
        expect(resThree).toBe(true);
    });

    test("get posts from database", async() => {
        let FirstPost = new Post(DB_HANDLER, "Adam", "First Comment", "ref_example");
        let SecondPost = new Post(DB_HANDLER, "Matt", "Awesome Movie");
        let ThirdPost = new Post(DB_HANDLER, "Steven", "Boring...");
        let resOne = await FirstPost.savePost();
        let resTwo = await SecondPost.savePost();
        let resThree = await ThirdPost.savePost();
        let examplePost = new Post(DB_HANDLER);
        let result = await examplePost.getPosts();
        expect(result).toBeInstanceOf(Array);
    });
});

describe("REST Tests", async () => {

    test("get posts", async() => {
        let res = await request(server).get("/comments");
        expect(res.status).toBe(200);
        expect(res.body).toBeInstanceOf(Array);
    });
    test("get movies",async() => {
        let res = await request(server).get("/movies");
        expect(res.status).toBe(200);
        expect(res.body).toBeInstanceOf(Array);
    });

    test("post comment", async() => {
        let res = await request(server).post("/comments").send({user: "TestUser", text: "ExampleComment", ref_num: "xxx"});
        expect(res.status).toBe(200);
    });
    test("post movie", async() => {
        let res = await request(server).post("/movies").send({title: "Batman"});
        expect(res.status).toBe(200);
    });

});